import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import 'package:flutter_redux_boilerplate/styles/colors.dart';
import 'package:flutter_redux_boilerplate/actions/auth_actions.dart';
import 'package:flutter_redux_boilerplate/models/app_state.dart';

import 'package:flutter_redux_boilerplate/screens/map_screen.dart';

class MainDrawer extends StatelessWidget {

    List<String> dropdown = ['first', 'second','third'];

    MainDrawer({Key key}): super(key: key);

    @override
    Widget build(BuildContext context) {
        return new StoreConnector<AppState, dynamic>(
            converter: (store) => (BuildContext context) { store.dispatch(logout(context)); },
            builder: (BuildContext context, logout) => new Drawer(
                child: new ListView(
                    children: <Widget>[
                      new UserAccountsDrawerHeader(
                        accountEmail: new Text("bobdraga@gmail.com"),
                        accountName: new Text("Bob Draga"),
                        currentAccountPicture: new CircleAvatar(
                          backgroundImage: new NetworkImage("https://avatars3.githubusercontent.com/u/16825392?s=460&v=4"),
                        ),
                        decoration: new BoxDecoration(
                          image: new DecorationImage(
                              image: new NetworkImage("https://img00.deviantart.net/35f0/i/2015/018/2/6/low_poly_landscape__the_river_cut_by_bv_designs-d8eib00.jpg"),
                              fit: BoxFit.fill
                          )
                        ),
                      ),

                    new ExpansionTile(
                        leading: new Icon(Icons.star),
                        title: new Text('Favorites'),
                        children: dropdown.map((val)=> new ListTile(title: new Text(val), onTap: () => print('$val'),)).toList(),
//                        onTap: () => print('you pressed favorites')
                    ),

                    new ListTile(
                        leading: new Icon(Icons.map),
                        title: new Text('Map'),
//                        onTap: () => new MapPage()
                        onTap: () {
                          Navigator.of(context).pop();
                          Navigator.of(context).pushNamed('/map');
                        }

                    ),
                    new ListTile(
                        leading: new Icon(Icons.supervised_user_circle),
                        title: new Text('Users'),
                        onTap: () => print('you pressed users')
                    ),
//                    new Divider(),
                    new ListTile(
                        leading: new Icon(Icons.exit_to_app),
                        title: new Text('Sign Out'),
                        onTap: () => logout(context)
                    ),
                    ],
                )
            )
        );
    }

}