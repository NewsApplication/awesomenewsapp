import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import 'package:flutter_redux_boilerplate/styles/colors.dart';
import 'package:flutter_redux_boilerplate/actions/auth_actions.dart';
import 'package:flutter_redux_boilerplate/models/app_state.dart';

import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';

class MapPage extends StatefulWidget {

//  MapPage({Key key}) : super(key: key);

  MapPageState createState() => new MapPageState();

}

class MapPageState extends State<MapPage>{
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(title: new Text('Leaflet Senegal Maps'),),
      body: new FlutterMap(
//          mapController: ,
        options: new MapOptions(
            center: new LatLng(14.69, -17.44),
            minZoom: 10.0
        ),
        layers: [
          new TileLayerOptions(
              urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
              subdomains: ['a','b','c']
          ),
          new MarkerLayerOptions(
              markers: [
                new Marker(
                    width: 45.0,
                    height: 45.0,
                    point: new LatLng(14.69, -17.44),
                    builder: (context) => new Container(
                      child: IconButton(
                          icon: Icon(Icons.location_on),
                          color: Colors.red,
                          iconSize: 45.0,
                          onPressed: () => print('tapped')
                      ),
                    )
                )
              ]
          )
        ],
      ),

    );
  }

}