import 'package:flutter/material.dart';

import 'package:flutter_redux_boilerplate/containers/login_form.dart';
import 'package:flutter_redux_boilerplate/containers/register_form.dart';
import 'package:flutter_redux_boilerplate/screens/resetPassword_screen.dart';
import 'package:flutter_redux_boilerplate/styles/colors.dart';

class RegisterScreen extends StatelessWidget {
  RegisterScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: new Container(
            child: new Padding(
                padding: new EdgeInsets.fromLTRB(32.0, MediaQuery
                    .of(context)
                    .padding
                    .top + 28.0, 32.0, 32.0),
                child: new Column(
                    children: <Widget>[RegisterForm()]
                )
            )
        )
    );
  }
}

