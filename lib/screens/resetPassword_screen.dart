import 'package:flutter/material.dart';

import 'package:flutter_redux_boilerplate/containers/login_form.dart';
import 'package:flutter_redux_boilerplate/containers/resetPassword_form.dart';
import 'package:flutter_redux_boilerplate/styles/colors.dart';

class ResetPasswordScreen extends StatelessWidget {
  ResetPasswordScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: new Container(
            padding: new EdgeInsets.fromLTRB(32.0, MediaQuery
                .of(context)
                .padding
                .top + 32.0, 32.0, 32.0),
          child: new Center(
            child: ResetPasswordForm(),
          )
            ),
         );
    }

}