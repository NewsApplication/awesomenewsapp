import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter_redux_boilerplate/models/news_model.dart';

class CategoriesTab extends StatefulWidget {
  @override
  CategoriesTabState createState() => CategoriesTabState();
}

class CategoriesTabState extends State<CategoriesTab> {
  List<News> newsList = [];

  Future<String> getData() async {
    String url = "http://10.6.35.178:3000/api/news";
    var req = await http.get(Uri.encodeFull(url), headers: {"tenant": "TSA"});

    print(req);

    setState(() {
      List list = jsonDecode(req.body);
      for (int i = 0; i < list.length; i++) {
        newsList.add(News.fromJsonMap(list[i]));
      }
    });

    print(newsList);
    return "Success";
  }

  Widget foodCategory() {
    return Scaffold(
        appBar: AppBar(title: Text('Food')),
        body: ListView(
            children: newsList.
            where((item) => item.type == 'Food')
                .map((item) => Text(item.title))
                .toList()));
//                .map((item) =>
//                    item.type == 'Food' ? Text(item.title) : Center(child: Text('Nu s-au gasit stiri')))
//                .toList()));
  }

  Widget styleCategory() {
    return Scaffold(
        appBar: AppBar(title: Text('Style')),
        body: ListView(
            children: newsList.
            where((item) => item.type == 'Style')
                .map((item) => Text(item.title))
                .toList()));
//                .map((item) =>
//            item.type == 'Style' ? Text(item.title) : Center(child: Text('Nu s-au gasit stiri')))
//                .toList()));
  }

  @override
  Widget build(BuildContext context) {
    return Center(
        child: ListView(children: <Widget>[
      ListTile(
          leading: Icon(Icons.fastfood),
          title: Text('Food'),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => foodCategory()),
            );
          }),
      ListTile(
          leading: Icon(Icons.style), title: Text('Style'),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => styleCategory()),
            );
          }
      )
    ]));
  }

  @override
  void initState() {
    this.getData();
    super.initState();
  }
}