import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_boilerplate/actions/news_actions.dart';
import 'package:flutter_redux_boilerplate/models/app_state.dart';
import 'package:flutter_redux_boilerplate/screens/one_news.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:math' as math;
import 'dart:async';

import 'package:flutter_redux_boilerplate/models/news_model.dart';
import 'package:redux/redux.dart';

class NewsTab extends StatefulWidget {
  @override
  NewsTabState createState() => NewsTabState();
}

class NewsTabState extends State<NewsTab> with TickerProviderStateMixin {

  List<AnimationController> controlersList = [];

  MaterialAccentColor color = Colors.greenAccent;
  List<News> newsList = [];
  String url = "http://195.216.218.54:3000/api/news";
  static const Base64Codec base64 = const Base64Codec();

  @override
  void initState() {
    super.initState();
  }

  Future<void> getData(Store<AppState> store) async {
    var req = await http.get(Uri.encodeFull(url), headers: {"tenant": "TSA"});

    setState(() {
      List list = jsonDecode(req.body);
      for (int i = 0; i < list.length; i++) {
        newsList.add(News.fromJsonMap(list[i]));
      }
    });

    store.dispatch(NewsAction(newsList));
  }

  Future<void> putData(id, index) async {
    Map<String, dynamic> map = {"favorite": newsList[index].favorites};

    await http.put(Uri.encodeFull(url.toString() + "/" + id.toString()),
        headers: {'Content-type': 'application/json', 'tenant': 'TSA'},
        body: jsonEncode(map));
  }

  void deleteData(String id, bool validateAlert, Store<AppState> store) async {
    if (validateAlert) {
      await http.delete(Uri.encodeFull(url.toString() + "/" + id),
          headers: {'Content-type': 'application/json', 'tenant': 'TSA'});

      newsList = [];
      await getData(store);
    }
  }

  void showAlert(String id, Store<AppState> store) {
    bool validateAlert = false;

    AlertDialog alert = AlertDialog(
      content: Text(
        "Are you sure you want to delete this news ?",
        textAlign: TextAlign.center,
      ),
      actions: <Widget>[
        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              FlatButton(
                  onPressed: () {
                    validateAlert = true;
                    Navigator.pop(context);
                    deleteData(id, validateAlert, store);
                  },
                  child: Text("Yes")),
              FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text("No"),
              )
            ],
          ),
        )
      ],
    );
    showDialog(context: context, child: alert);
  }

  void turnAround(index) {
    if(controlersList[index].isCompleted)
      controlersList[index].reverse();
    else
      controlersList[index].forward();
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, Store>(
        onInit: (store) async {
      await getData(store);

      for (int i = 0; i < newsList.length; i++) {
        controlersList.add(AnimationController(
            vsync: this, duration: Duration(milliseconds: 1000)));
      }
    }, converter: (store) {
      return store;
    }, builder: (BuildContext context, store) {
      return Center(
          child: ListView.builder(
        itemBuilder: (BuildContext context, int index) =>
            _makeListElement(index, store),
      ));
    });
  }

  Widget _makeListElement(int index, Store<AppState> store) {
    double begin;
    double end;

    if (index == newsList.length) {
      return null;
    }
    if (index != null && newsList.length != 0) {
      if (newsList[index].favorites) {
        begin = 1.0;
        end = 0.0;
      } else {
        begin = 0.0;
        end = 1.0;
      }
    }
    return Container(
        decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(
                    style: BorderStyle.solid,
                    width: 1.0,
                    color: Colors.white))),
        margin: EdgeInsets.all(5.0),
        padding: EdgeInsets.all(5.0),
        child: Column(
          children: <Widget>[
            Text(newsList[index].title, textScaleFactor: 2.0),
            Image.memory(base64.decode(newsList[index].img)),
            ExpansionTile(
              title: Text("Summary", textScaleFactor: 1.5),
              children: <Widget>[
                Text(newsList[index].summary, textScaleFactor: 1.5),
                GestureDetector(
                    behavior: HitTestBehavior.opaque,
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  OneNews(id: index, newsList: newsList)));
                    },
                    child: ButtonBar(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(
                          "Want to read more?",
                          textScaleFactor: 1.5,
                          style: TextStyle(color: color),
                        ),
                        IconButton(
                            icon: Icon(
                              Icons.arrow_forward,
                              color: color,
                              size: 30.0,
                            ),
                            onPressed: null)
                      ],
                    )),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    AnimatedBuilder(
                        animation: controlersList[index],
                        child: IconButton(
                            icon: Icon(Icons.favorite_border),
                            onPressed: () {
                              newsList[index].favorites =
                                  !newsList[index].favorites;
                              turnAround(index);
                              putData(newsList[index].id, index);
                            },
                            color: color,
                            tooltip: "Favorite"),
                        builder: (context, child) => Stack(
                              children: <Widget>[
                                child,
                                Opacity(
                                    child: IconButton(
                                        icon: Icon(Icons.favorite),
                                        onPressed: () {
                                          newsList[index].favorites =
                                              !newsList[index].favorites;
                                          turnAround(index);
                                          putData(newsList[index].id, index);
                                        },
                                        color: color,
                                        tooltip: "Favorite"),
                                    opacity:
                                        Tween<double>(begin: begin, end: end)
                                            .animate(controlersList[index])
                                            .value)
                              ],
                            )),
                    Text("Favorite", style: TextStyle(color: color))
                  ],
                ),
                Column(
                  children: <Widget>[
                    IconButton(
                        icon: Icon(Icons.edit),
                        onPressed: () {
                          store.dispatch(NewsAction(newsList, index: index));
                          Navigator.pushNamed(context, '/edit');
                        },
                        color: color,
                        tooltip: "Edit"),
                    Text("Edit", style: TextStyle(color: color))
                  ],
                ),
                Column(
                  children: <Widget>[
                    IconButton(
                        icon: Icon(Icons.delete),
                        onPressed: () {
                          showAlert(newsList[index].id, store);
                        },
                        color: color,
                        tooltip: "Delete"),
                    Text("Delete", style: TextStyle(color: color))
                  ],
                ),
              ],
            ),
          ],
        ));
  }
}
