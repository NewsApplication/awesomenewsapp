import 'package:flutter/material.dart';
import 'package:flutter_redux_boilerplate/presentation/platform_adaptive.dart';

class SearchTab extends StatelessWidget {
    SearchTab({Key key}) : super(key: key);

    @override
    Widget build(BuildContext context) {
        return Column(
            children: [
            Text('Search'),
              TextFormField(
                decoration:  InputDecoration(labelText: 'Search for news:'),
                validator: (val) =>
                val.isEmpty ? 'Enter name.' : null,
              ),
               new Padding(
                 padding: new EdgeInsets.only(top: 20.0),
                 child: PlatformAdaptiveButton(
                   onPressed:() {

                   },
                   icon: Icon(Icons.search),
                   child: Text('Search'),
                 ),
               )

            ]
        );
    }
}