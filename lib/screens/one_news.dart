import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_redux_boilerplate/models/news_model.dart';
import 'package:flutter_redux_boilerplate/presentation/platform_adaptive.dart';
import 'package:flutter_redux_boilerplate/containers/comment_form.dart';

class OneNews extends StatelessWidget {

  final int id;
  final List<News> newsList;
  static const Base64Codec base64 = const Base64Codec();

  OneNews({Key key, this.id, this.newsList}) : super(key: key);

  String toPrettyDate(String s){
    DateTime date = DateTime.parse(s);
    String hour = date.hour.toString();
    String minutes = date.minute.toString();
    String year = date.year.toString();
    String month = date.month.toString();
    String day = date.day.toString();

    return "$hour:$minutes  $day/$month/$year";
  }

  @override
  Widget build(BuildContext context) {
    final List<Comments> comments = newsList[id].comments;

    return Scaffold(
      backgroundColor: Colors.white10,
      appBar: new PlatformAdaptiveAppBar(
        platform: Theme.of(context).platform,

      ),
      body: ListView.builder(

        itemCount: newsList == null ? 0 : 1,
          itemBuilder: (BuildContext context, index) => Container(
            margin: EdgeInsets.fromLTRB(10.0, 25.0, 10.0, 20.0),
            padding: EdgeInsets.all(5.0),
            child: Column(
              verticalDirection: VerticalDirection.down,
              children: <Widget>[
                Text(newsList[id].title, textScaleFactor: 2.0),
                Image.memory(base64.decode(newsList[id].img)),
                Text(newsList[id].description, textScaleFactor: 1.5),
                ExpansionTile(
                    title: Text("Show comments"),
                    children: <Widget>[
                      ListView.builder(
                        physics: ScrollPhysics(parent: NeverScrollableScrollPhysics()),
                        shrinkWrap: true,
                          itemCount: comments == null ? 0 : comments.length,
                          itemBuilder: (BuildContext context, index) => Container(
                              margin: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
                              padding: EdgeInsets.all(5.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text("User: " + comments[index].userName, textScaleFactor: 1.3),
                                  Padding(
                                      padding: EdgeInsets.only(top: 10.0, bottom: 15.0),
                                      child: Text(comments[index].comment, textScaleFactor: 1.5,)
                                  ),
                                  Row(children: <Widget>[Text(toPrettyDate(comments[index].date), textScaleFactor: 1.3)], mainAxisAlignment: MainAxisAlignment.end,),
                                ],
                              )
                          )
                      ),
                      GestureDetector(
                          onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => CommentForm(id: newsList[id].id, newsList: newsList, index: id))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              IconButton(icon: Icon(Icons.comment), onPressed: null),
                              Text("Add comment", textScaleFactor: 1.5,)
                            ],)
                      )
                    ],
                )
              ],
            ),
          )
      )
    );
  }
}