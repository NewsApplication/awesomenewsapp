import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_boilerplate/containers/add_form.dart';
import 'package:flutter_redux_boilerplate/containers/edit_form.dart';
import 'package:redux_persist_flutter/redux_persist_flutter.dart';

import 'package:flutter_redux_boilerplate/presentation/platform_adaptive.dart';
import 'package:flutter_redux_boilerplate/screens/loading_screen.dart';
import 'package:flutter_redux_boilerplate/screens/login_screen.dart';
import 'package:flutter_redux_boilerplate/screens/main_screen.dart';
import 'package:flutter_redux_boilerplate/store/store.dart';
import 'package:flutter_redux_boilerplate/middleware/middleware.dart';
import 'package:flutter_redux_boilerplate/models/app_state.dart';

import 'package:flutter_redux_boilerplate/screens/map_screen.dart';
import 'package:flutter_redux_boilerplate/screens/one_news.dart';
//import 'package:flutter_redux_boilerplate/containers/add_form.dart';

void main() => runApp(new ReduxApp());

class ReduxApp extends StatelessWidget {
    final store = createStore();


    ReduxApp();

    @override
    Widget build(BuildContext context) {
        return new PersistorGate(
            persistor: persistor,
            loading: new LoadingScreen(),
            builder: (context) => new StoreProvider<AppState>(
                store: store,
                child: new MaterialApp(
                    title: 'ReduxApp',
                    theme: defaultTargetPlatform == TargetPlatform.iOS
                        ? kIOSTheme
                        : ThemeData.dark(),
                routes: <String, WidgetBuilder>{
                    '/': (BuildContext context) => new StoreConnector<AppState, dynamic>( 
                        converter: (store) => store.state.auth.isAuthenticated, 
                        builder: (BuildContext context, isAuthenticated) => isAuthenticated ? new MainScreen() : new LoginScreen()
                    ),
                    '/login': (BuildContext context) => new LoginScreen(),
                    '/main': (BuildContext context) => new MainScreen(),
                    '/map' : (BuildContext context) => new MapPage(),
                    '/edit' : (BuildContext context) => EditForm(),
                    '/add' : (BuildContext context) => AddForm(),
//                    '/add' : (BuildContext context) => new AddForm()
                }
                )
            ),
        );
    }
}
