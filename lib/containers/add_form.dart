import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_boilerplate/models/news_model.dart';
import 'package:flutter_redux_boilerplate/models/news_state.dart';
import 'package:flutter_redux_boilerplate/screens/loading_screen.dart';
import 'package:redux/redux.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:flutter_redux_boilerplate/presentation/platform_adaptive.dart';
import 'package:flutter_redux_boilerplate/models/app_state.dart';
import 'package:image_picker/image_picker.dart';

class AddForm extends StatefulWidget {
  @override
  _AddState createState() => new _AddState();
}

class _AddState extends State<AddForm> {

  final formKey = GlobalKey<FormState>();
  String title;
  String summary;
  String description;
  String type;
  List locations = [];
  Location location = Location();
  int likes = 0;
  int dislikes = 0;
  bool favorite = false;
  int views = 0;
  File imageFromPhone;
  List<Comments> comments = [];
  String url = "http://195.216.218.54:3000/api/news";
  static const Base64Codec base64 = const Base64Codec();

  Future<void> getImage() async {

    imageFromPhone = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
    });
  }


  void _submit(img, store) {
    final form = formKey.currentState;

    if (form.validate()) {
      form.save();
      saveNews(img, store);
    }
  }

  Future<void> saveNews(img, store) async {

    locations.add(Location().toJson(location));
//    locations.map((f) => Location().toJson(f)).toList();
    Map<String, dynamic> map = {
      "title": title,
      "summary": summary,
      "description": description,
      "type": type,
      "comments": comments,
      "location": locations,
      "likes": likes,
      "dislikes": dislikes,
      "views": views,
      "favorite": favorite,
      "img": imageFromPhone == null ? img : Base64Encoder().convert(imageFromPhone.readAsBytesSync())
    };

    await http.post(Uri.encodeFull(url.toString()),
        headers: {'Content-type': 'application/json', 'tenant': 'TSA'},
        body: jsonEncode(map));

    Navigator.popAndPushNamed(context, "/main");

  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, Store<AppState>>(
        converter: (Store<AppState> store) {
          return store;
        },
        builder: (BuildContext context, store) {
          NewsState news = store.state.newsState;
          int index = news.index == null ? 0 : news.index;


          return Scaffold(
              appBar: PlatformAdaptiveAppBar(
                platform: Theme.of(context).platform,

              ),
              body: ListView(
                children: <Widget>[
                  Form(
                    key: formKey,
                    child: new Column(
                      children: [
                        TextFormField(
                          decoration: InputDecoration(labelText: 'Title'),
                          validator: (title) { if(title.isEmpty) return 'Please enter a title.';},
                          onSaved: (title) => this.title = title,
                        ),

                        TextFormField(
                          decoration: InputDecoration(labelText: 'Summary'),
                          validator: (summary) => summary.isEmpty ? 'Please enter a summary' : null,
                          onSaved: (summary) => this.summary = summary,
                          maxLines: 5,
                        ),
                        TextFormField(
                          decoration: InputDecoration(labelText: 'Description'),
                          validator: (description) => description.isEmpty ? 'Please enter a description' : null,
                          onSaved: (description) => this.description = description,
                          maxLines: 10,
                        ),
                        TextFormField(
                          decoration: InputDecoration(labelText: 'Type'),
                          validator: (type) => type.isEmpty ? 'Please enter a type' : null,
                          onSaved: (type) => this.type = type,
                          maxLines: 1,
                        ),
                        TextFormField(
                          decoration: InputDecoration(labelText: 'lat'),
                          validator: (lat) => lat.isEmpty ? 'Please enter an latitude number' : null,
                          onSaved: (lat) => this.location.lat = lat,
                        ),
                        TextFormField(
                          decoration: InputDecoration(labelText: 'lat'),
                          validator: (long) => long.isEmpty ? 'Please enter an longitude number' : null,
                          onSaved: (long) => this.location.long = long,
                        ),
                        Text("Photo preview",textScaleFactor: 1.5),
                        imageFromPhone == null ?
                        Image.memory(base64.decode(news.newsList[index].img)) :
                        Image.file(imageFromPhone),
                        IconButton(icon: Icon(Icons.file_upload), onPressed: () => getImage()),
                        MaterialButton(onPressed: () {
                          Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => LoadingScreen()));
                          _submit(news.newsList[index].img, store);
                        },
                            child: Text("Submit form", textScaleFactor: 1.5,))
                      ],
                    ),
                  ),
                ],)
          );
        }
    );
  }
}