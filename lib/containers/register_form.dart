import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

import 'package:flutter_redux_boilerplate/presentation/platform_adaptive.dart';
import 'package:flutter_redux_boilerplate/models/app_state.dart';
import 'package:flutter_redux_boilerplate/actions/auth_actions.dart';

class RegisterForm extends StatefulWidget {
  @override
  _RegisterFormState createState() => new _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  final formKey = new GlobalKey<FormState>();

  String _username;
  String _password;
  String _email;
  String _passwordRpt;


  void _submit() {
    final form = formKey.currentState;

    if (form.validate()) {
      form.save();
    }
  }

  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, dynamic>(
        converter: (Store<AppState> store) {
          return (BuildContext context, String username, String password, String passwordRpt, String email) =>
              store.dispatch(register(context, username, password, passwordRpt, email));
        },
        builder: (BuildContext context, register) {
          return new Form(
            key: formKey,
            child: new Column(
              children: [
                new TextFormField(
                  decoration: new InputDecoration(labelText: 'Username'),
                  validator: (val) =>
                  val.isEmpty ? 'Please enter your username.' : null,
                  onSaved: (val) => _username = val,
                ),
                new TextFormField(
                  decoration: new InputDecoration(labelText: 'Password'),
                  validator: (val) =>
                  val.isEmpty ? 'Please enter your password.' : null,
                  onSaved: (val) => _password = val,
                  obscureText: true,
                ),
                new TextFormField(
                  decoration: new InputDecoration(labelText: 'Repeat Password'),
                  validator: (val) =>
                  val.isEmpty ? 'Please confirm your password.' : null,
                  onSaved: (val) => _passwordRpt = val,
                  obscureText: true,
                ),
                new TextFormField(
                  decoration: new InputDecoration(labelText: 'Email'),
                  validator: (val) =>
                  val.isEmpty ? 'Please enter your email.' : null,
                  onSaved: (val) => _email = val,
                  obscureText: true,
                ),
                new Padding(
                  padding: new EdgeInsets.only(top: 20.0),
                  child: new PlatformAdaptiveButton(
                    onPressed:() {
                      _submit();
                      register(context, _username, _password, _passwordRpt, _email);
                    },
                    icon: new Icon(Icons.done),
                    child: new Text('Register'),
                      onTap: () => print('Successfully registered!')
                  ),
                )
              ],
            ),
          );
        }
    );
  }

}