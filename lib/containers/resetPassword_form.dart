import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

import 'package:flutter_redux_boilerplate/presentation/platform_adaptive.dart';
import 'package:flutter_redux_boilerplate/models/app_state.dart';
import 'package:flutter_redux_boilerplate/actions/auth_actions.dart';

class ResetPasswordForm extends StatefulWidget {
  @override
  _ResetPasswordState createState() => new _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPasswordForm> {
  final formKey = new GlobalKey<FormState>();

  String _email;

  void _submit() {
    final form = formKey.currentState;

    if (form.validate()) {
      form.save();
    }
  }

  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, dynamic>(
        converter: (Store<AppState> store) {
          return (BuildContext context, String username, String password) =>
              store.dispatch(login(context, username, password));
        },
        builder: (BuildContext context, resetPassword) {
          return new Form(
            key: formKey,
            child: new Column(
              children: [
                new TextFormField(
                  decoration: new InputDecoration(labelText: 'Email associated with account:'),
                  validator: (val) =>
                  val.isEmpty ? 'Please enter your email.' : null,
                  onSaved: (val) => _email = val,
                ),
                new Padding(
                  padding: new EdgeInsets.only(top: 20.0),
                  child: new PlatformAdaptiveButton(
                    onPressed:() {
                      _submit();
                      resetPassword(context, _email);
                    },
                    icon: new Icon(Icons.done),
                    child: new Text('Reset Password'),
                      onTap: () => print('Password reset email sent!')
                  ),
                )
              ],
            ),
          );
        }
    );
  }

}