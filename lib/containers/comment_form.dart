import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_boilerplate/models/news_state.dart';
import 'package:flutter_redux_boilerplate/presentation/platform_adaptive.dart';
import 'package:flutter_redux_boilerplate/screens/loading_screen.dart';
import 'package:flutter_redux_boilerplate/models/news_model.dart';
import 'package:flutter_redux_boilerplate/screens/one_news.dart';
import 'package:redux/redux.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

String globalId;
List<News> globalNewsList;
int globalIndex;

class CommentForm extends StatefulWidget{

  //To Do Gaseste o cale sa transmiti astea de aici in clasa de mai jos ! !

  CommentForm({String id, List<News> newsList, int index}){globalId = id; globalNewsList = newsList; globalIndex = index;}

  _CommentFormState createState() => _CommentFormState();
}

class _CommentFormState extends State<CommentForm>{

  final formKey = new GlobalKey<FormState>();
  Comments comment = Comments();
  String url = "http://195.216.218.54:3000/api/news";

  void _submit(){
    final form = formKey.currentState;

    if (form.validate()){
      form.save();
      postComment();
    }
  }

  Future<void> postComment() async{
    comment.userName = "Bob Draga";
    comment.date = DateTime.now().toString();
    comment.dislikes = 0;
    comment.likes = 0;

    globalNewsList[globalIndex].comments.add(comment);

    List commentsList = globalNewsList[globalIndex].comments.map((e) => Comments().toJson(e)).toList();

    Map<String, dynamic> map ={"comments": commentsList};

    await http.put(Uri.encodeFull(url.toString() + "/" + globalId.toString()),
        headers: {'Content-type': 'application/json', 'tenant': 'TSA'},
        body: jsonEncode(map));

    await Navigator.pop(context);

  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: PlatformAdaptiveAppBar(
          platform: Theme.of(context).platform,
        ),
        body: ListView(
          children: <Widget>[
            Form(
              key: formKey,
              child: new Column(
                children: [
                  TextFormField(
                    decoration: InputDecoration(labelText: 'Your comment'),
                    validator: (comment) { if(comment.isEmpty) return 'Please write something.';},
                    onSaved: (comment) => this.comment.comment = comment,
                    maxLines: 5,
                  ),
                  GestureDetector(
                    onTap: () => _submit(),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Text("Post", textScaleFactor: 1.5,),
                        IconButton(icon: Icon(Icons.send), onPressed: null)
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],)
    );
  }
}