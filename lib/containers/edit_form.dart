import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_boilerplate/models/news_state.dart';
import 'package:flutter_redux_boilerplate/screens/loading_screen.dart';
import 'package:redux/redux.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:flutter_redux_boilerplate/presentation/platform_adaptive.dart';
import 'package:flutter_redux_boilerplate/models/app_state.dart';
import 'package:image_picker/image_picker.dart';

class EditForm extends StatefulWidget {
  @override
  _EditState createState() => new _EditState();
}

class _EditState extends State<EditForm> {

  final formKey = new GlobalKey<FormState>();
  String title;
  String summary;
  String description;
  String type;
  File imageFromPhone;
  String url = "http://195.216.218.54:3000/api/news";
  static const Base64Codec base64 = const Base64Codec();

  Future<void> getImage() async {

    imageFromPhone = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
    });
  }


  void _submit(String id, img, store) {
    final form = formKey.currentState;

    if (form.validate()) {
      form.save();
      updateNews(id, img, store);
    }
  }

  Future<void> updateNews(String id, img, store) async {
    Map<String, dynamic> map = {
      "title": title,
      "summary": summary,
      "description": description,
      "type": type,
      "img": imageFromPhone == null ? img : Base64Encoder().convert(imageFromPhone.readAsBytesSync())
    };

     await http.put(Uri.encodeFull(url.toString() + "/" + id.toString()),
        headers: {'Content-type': 'application/json', 'tenant': 'TSA'},
        body: jsonEncode(map));

      Navigator.popAndPushNamed(context, "/main");

  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, Store<AppState>>(
        converter: (Store<AppState> store) {
          return store;
        },
        builder: (BuildContext context, store) {
          NewsState news = store.state.newsState;
          int index = news.index == null ? 0 : news.index;


          return Scaffold(
            appBar: PlatformAdaptiveAppBar(
              platform: Theme.of(context).platform,

            ),
            body: ListView(
              children: <Widget>[
                Form(
                  key: formKey,
                  child: new Column(
                    children: [
                      TextFormField(
                        decoration: InputDecoration(labelText: 'Title'),
                        initialValue: news.newsList[index].title,
                        validator: (title) { if(title.isEmpty) return 'Please enter a title.';},
                        onSaved: (title) => this.title = title,
                      ),

                      TextFormField(
                        decoration: InputDecoration(labelText: 'Summary'),
                        initialValue: news.newsList[index].summary,
                        validator: (summary) => summary.isEmpty ? 'Please enter a summary' : null,
                        onSaved: (summary) => this.summary = summary,
                        maxLines: 5,
                      ),
                      TextFormField(
                        decoration: InputDecoration(labelText: 'Description'),
                        initialValue: news.newsList[index].description,
                        validator: (description) => description.isEmpty ? 'Please enter a description' : null,
                        onSaved: (description) => this.description = description,
                        maxLines: 10,
                      ),
                      TextFormField(
                        decoration: InputDecoration(labelText: 'Type'),
                        initialValue: news.newsList[index].type,
                        validator: (type) => type.isEmpty ? 'Please enter a type' : null,
                        onSaved: (type) => this.type = type,
                        maxLines: 1,
                      ),
                      Text("Photo preview",textScaleFactor: 1.5),
                      imageFromPhone == null ?
                      Image.memory(base64.decode(news.newsList[index].img)) :
                          Image.file(imageFromPhone),
                      IconButton(icon: Icon(Icons.file_upload), onPressed: () => getImage()),
                      MaterialButton(onPressed: () {
                        Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => LoadingScreen()));
                        _submit(news.newsList[index].id, news.newsList[index].img, store);
                      },
                          child: Text("Submit form", textScaleFactor: 1.5,))
                    ],
                  ),
                ),
            ],)
          );
        }
    );
  }
}