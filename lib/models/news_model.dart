import 'package:json_annotation/json_annotation.dart';
part 'news_model.g.dart';

@JsonSerializable()
class Location {

  Location({lat, long}){
    this.lat = lat;
    this.long = long;
  }
  String lat;
  String long;

  factory Location.fromJson(Map<String, dynamic> json) => _$LocationFromJson(json);

  Map<String, dynamic> toJson(Location location) => _$LocationToJson(location);

  @override
  String toString() {
    return 'lat: $lat, long: $long';
  }
}

@JsonSerializable()
class Comments {

  Comments({likes, dislikes, userName, comment, date}){
    this.dislikes = dislikes;
    this.likes = likes;
    this.comment = comment;
    this.date = date;
    this.userName = userName;
  }
  int likes;
  int dislikes;
  String userName;
  String comment;
  String date;

  factory Comments.fromJson(Map<String, dynamic> json) => _$CommentsFromJson(json);

  Map<String, dynamic> toJson(Comments comments) => _$CommentsToJson(comments);

  @override
  String toString() {
    return 'likes: $likes,'
    'dislikes: $dislikes,'
    'userName: $userName,'
    'comment: $comment,'
    'date: $date';
  }
}

@JsonSerializable()
class News {
  String id;
  String title;
  String summary;
  String description;
  List<Location> location;
  String img;
  String type;
  List<Comments> comments;
  int likes;
  int dislikes;
  bool favorites;
  int views;

  News({this.title, this.summary, this. description, this.location, this.img, this.type, this.comments, this.likes, this.dislikes, this.favorites, this.views, this.id});

  factory News.fromJsonMap(Map<String, dynamic> json) => _$NewsFromJson(json);

  Map<String, dynamic> toJson(News news) => _$NewsToJson(news);

  @override
  String toString() {
    return '{title: $title,'
          'summary: $summary,'
          'description: $description'
          'location: $location'
          'comments: $comments}';
  }
}