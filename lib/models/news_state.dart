import 'package:flutter_redux_boilerplate/models/news_model.dart';

class NewsState {

  final List<News> newsList;
  final index;

  NewsState({this.newsList, this.index});

  factory NewsState.fromJSON(Map<String, dynamic> json) => NewsState (
    newsList: json['newsList'] == null ? null : json['newsList'].map((news) => news == null ? null : News.fromJsonMap(news)).toList()
  );

  Map<String, dynamic> toJSON() => <String, dynamic>{
    'newsList': this.newsList == null ? null : this.newsList.map((news) => news == null ? null : News().toJson(news)).toList()
  };

}