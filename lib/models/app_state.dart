import 'package:meta/meta.dart';

import 'package:flutter_redux_boilerplate/models/auth_state.dart';
import 'package:flutter_redux_boilerplate/models/news_state.dart';

@immutable
class AppState {
    final AuthState auth;
    final NewsState newsState;

    AppState({AuthState auth, NewsState newsState}):
        auth = auth ?? new AuthState(),
        newsState = newsState ?? new NewsState();


    static AppState rehydrationJSON(dynamic json) => new AppState(
        auth: AuthState.fromJSON(json['auth']),
    );

    Map<String, dynamic> toJson() => {
        'auth': auth.toJSON()
    };

    AppState copyWith({
        bool rehydrated,
        AuthState auth,
        NewsState newsState
    }) {
        return new AppState(
            auth: auth ?? this.auth,
            newsState: newsState ?? this.newsState
        );
    }

    @override
    String toString() {
        return '''AppState{
            auth: $auth,
            newsState: $newsState
        }''';
    }
}