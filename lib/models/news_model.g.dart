// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'news_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Location _$LocationFromJson(Map<String, dynamic> json) {
  return Location(lat: json['lat'], long: json['long']);
}

Map<String, dynamic> _$LocationToJson(Location instance) =>
    <String, dynamic>{'lat': instance.lat, 'long': instance.long};

Comments _$CommentsFromJson(Map<String, dynamic> json) {
  return Comments(
      likes: json['likes'],
      dislikes: json['dislikes'],
      userName: json['userName'],
      comment: json['comment'],
      date: json['date']);
}

Map<String, dynamic> _$CommentsToJson(Comments instance) => <String, dynamic>{
      'likes': instance.likes,
      'dislikes': instance.dislikes,
      'userName': instance.userName,
      'comment': instance.comment,
      'date': instance.date
    };

News _$NewsFromJson(Map<String, dynamic> json) {
  return News(
      title: json['title'] as String,
      summary: json['summary'] as String,
      description: json['description'] as String,
      location: (json['location'] as List)
          ?.map((e) =>
              e == null ? null : Location.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      img: json['img'] as String,
      type: json['type'] as String,
      comments: (json['comments'] as List)
          ?.map((e) =>
              e == null ? null : Comments.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      likes: json['likes'] as int,
      dislikes: json['dislikes'] as int,
      favorites: json['favorite'] as bool,
      views: json['views'] as int,
      id: json['_id'] as String);
}

Map<String, dynamic> _$NewsToJson(News instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'summary': instance.summary,
      'description': instance.description,
      'location': instance.location.map((e) => Location().toJson(e)).toList(),
      'img': instance.img,
      'type': instance.type,
      'comments': instance.comments.map((e) => Comments().toJson(e)).toList(),
      'likes': instance.likes,
      'dislikes': instance.dislikes,
      'favorites': instance.favorites,
      'views': instance.views
    };
