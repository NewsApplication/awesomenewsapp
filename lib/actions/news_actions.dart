import 'package:flutter_redux_boilerplate/models/news_model.dart';

class NewsAction {
  final List<News> newsList;
  final index;

  NewsAction(this.newsList,{this.index});
}
class EditAction {}
class DeleteAction{}