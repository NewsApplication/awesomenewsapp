import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

import 'package:flutter_redux_boilerplate/models/user.dart';
import 'package:flutter_redux_boilerplate/models/app_state.dart';

class UserLoginRequest {}

class UserRegisterRequest{}

class UserLoginSuccess {
    final User user;

    UserLoginSuccess(this.user);
}

class UserLoginFailure {
    final String error;

    UserLoginFailure(this.error);
}

class ResetPassword {

  final ResetPassword resetPassword;
    ResetPassword(this.resetPassword);

}
class UserRegisterSuccess{

  final String username;
  final String password;
  final String passwordRpt;
  UserRegisterSuccess(this.username, this.password, this.passwordRpt);

}

class UserRegisterFailure{

  final String error;

  UserRegisterFailure(this.error);

}
class UserLogout {}

final Function login = (BuildContext context, String username, String password) {
    return (Store<AppState> store) {
        store.dispatch(UserLoginRequest());
        if (username == 'asd' && password == 'asd') {
            store.dispatch(UserLoginSuccess(User('placeholder_token', 'placeholder_id')));
            Navigator.of(context).pushNamedAndRemoveUntil('/main', (_) => false);
        } else {
            store.dispatch(UserLoginFailure('Username or password were incorrect.'));
        }
    };
};
final Function resetPassword = (BuildContext context, String email){
    return (Store<AppState> store){
        store.dispatch(ResetPassword);
    };
};
final Function logout = (BuildContext context) {
    return (Store<AppState> store) {
        store.dispatch(UserLogout());
        Navigator.of(context).pushNamedAndRemoveUntil('/login', (_) => false);
    };
};
final Function register = (BuildContext context, String username, String password, String passwordRpt, String email){
  return (Store<AppState> store){
    store.dispatch(UserRegisterRequest());
    if (password == passwordRpt && username != 'asd') {
      store.dispatch(UserRegisterSuccess(username, password, passwordRpt));

      Navigator.of(context).pushNamedAndRemoveUntil('/login', (_) => false);
    }
    else {
      AlertDialog registerFailure = AlertDialog(
          content: Text("Passwords do not match!", textAlign: TextAlign.center,),
      );
      showDialog(context: context, child: registerFailure);
    }
  };
};