import 'package:flutter_redux_boilerplate/models/news_state.dart';

NewsState newsData(NewsState newsState, action) {
    return NewsState(newsList: action.newsList, index: action.index == null ? null : action.index);
}